﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTASARadioExternal
{
	public class Gta3Game : GtaGame
	{
		private const int RADIO_OFF = 11; // includes ambulance, fire, ice cream truck
		private const int RADIO_OFF2 = 12; // seems to be used in 2nd/3rd island
		private const int RADIO_HEAD = 0;
		private const int RADIO_CLEF = 1;
		private const int RADIO_JAH = 2;
		private const int RADIO_RISE = 3;
		private const int RADIO_LIPS = 4;
		private const int RADIO_GAME = 5;
		private const int RADIO_MSX = 6;
		private const int RADIO_FLB = 7;
		private const int RADIO_CHAT = 8;
		private const int RADIO_MP3 = 9; // probably
		private const int RADIO_COP = 10;
		private const int RADIO_MENU = 197; // also includes cutscenes and mission success jingle
		private const int RADIO_ALERT1 = 13; // inferred from code
		private const int RADIO_ALERT2 = 14; // inferred from code

		public bool PlayDuringRadio { get; set; }
		public bool PlayInPoliceCar { get; set; }
		public bool PlayInMenu { get; set; }
		public bool PlayDuringAlert { get; set; }
		public bool PlayWhileOff { get; set; }

		public Gta3Game(Process proc, int radAddr) : base(proc, radAddr)
		{
			// TODO gta3game constructor
		}

		public override bool ShouldRadioPlay()
		{
			var radioStatus = ReadRadioStatus();

			switch (radioStatus)
			{
				case RADIO_CHAT:
				case RADIO_CLEF:
				case RADIO_FLB:
				case RADIO_GAME:
				case RADIO_HEAD:
				case RADIO_JAH:
				case RADIO_LIPS:
				case RADIO_MSX:
				case RADIO_RISE:
				case RADIO_MP3:
					return PlayDuringRadio;
				case RADIO_MENU:
					return PlayInMenu;
				case RADIO_COP:
					return PlayInPoliceCar;
				case RADIO_ALERT1:
				case RADIO_ALERT2:
					return PlayDuringAlert;
				case RADIO_OFF:
				case RADIO_OFF2:
					return PlayWhileOff;
				case RADIO_DEFAULT:
				default:
					log.Warn("Unexpected radio status {0}", radioStatus);
					return true;
			}
		}

		public override float DesiredVolume()
		{
			switch (RadioStatus)
			{
				case RADIO_CHAT:
				case RADIO_CLEF:
				case RADIO_FLB:
				case RADIO_GAME:
				case RADIO_HEAD:
				case RADIO_JAH:
				case RADIO_LIPS:
				case RADIO_MSX:
				case RADIO_RISE:
				case RADIO_MP3:
					return 1.0f;
				case RADIO_MENU:
					return 0.2f;
				case RADIO_COP:
					return 1.0f;
				case RADIO_ALERT1:
				case RADIO_ALERT2:
					return 0.1f;
				case RADIO_OFF:
				case RADIO_OFF2:
					return 0.5f;
				case RADIO_DEFAULT:
				default:
					log.Warn("Unexpected radio status {0}", RadioStatus);
					return 1.0f;

			}
		}
	}
}
