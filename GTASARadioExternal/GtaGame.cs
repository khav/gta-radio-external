﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTASARadioExternal
{
	public abstract class GtaGame
	{
		protected static readonly Logger log = LogManager.GetCurrentClassLogger();

		protected const int RADIO_DEFAULT = -1;

		private readonly Process gameProcess;
		private readonly int radioAddress;

		public enum GameStatus
		{
			// TODO remove unused ones
			Unitialized, Shutdown, Running, Unrecognized, Unconfirmed, Playing, Silent, Error
		}

		public int RadioStatus { get; private set; }
		public GameStatus Status { get; private set; }

		protected GtaGame(Process proc, int radAddr)
		{
			gameProcess = proc;
			radioAddress = radAddr;
			Status = GameStatus.Running;
		}

		protected void ShutdownGame()
		{
			Status = GameStatus.Shutdown;
		}

		protected int ReadRadioStatus()
		{
			if (Status == GameStatus.Shutdown)
			{
				return RADIO_DEFAULT;
			}

			try
			{
				// XXX this will error out in NoGame if it's ever called
				var result = ReadMemory.ReadValue(gameProcess.Handle, radioAddress, false, false);
				if (result != RadioStatus)
				{
					log.Debug("Radio changed from {0} to {1}", RadioStatus, result);
					RadioStatus = result;
				}

				return result;
			}
			catch (Exception e)
			{
				// TODO put back specific exception handling if necessary
				// for InvalidOperationException, NullReferenceException, IndexOutOfRangeException 
				log.Error(e, "Exception while reading radio status");
				ShutdownGame();
				return RADIO_DEFAULT;
			}
		}

		public abstract bool ShouldRadioPlay();

		public abstract float DesiredVolume();
	}
}
