﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using NLog;

namespace GTASARadioExternal
{
	public partial class Form1 : Form
	{

		private static readonly Logger log = LogManager.GetCurrentClassLogger();

		Timer timer2;

		public enum displayedTexts { Unitialized, Shutdown, Running, Unrecognized, Unconfirmed, NoMusicPlayer }
		displayedTexts displayedText = displayedTexts.Unitialized;

		public static ReadMemory readMemory = new ReadMemory();


		public Form1()
		{
			InitializeComponent();

			statusLbl.Text = "Tool not configured";

			readMemory.InitTimer();        // run the timer that checks for updates
			WindowTimer();              // run the timer that prints these updates
		}

		void CheckGame()
		{
			// Check if the game still exists
			if (radioButtonSA.Checked)
			{
				readMemory.DetermineGameVersionSA();
			}
			else if (radioButtonVC.Checked)
			{
				readMemory.DetermineGameVersionVC();
			}
			else if (radioButtonIII.Checked)
			{
				readMemory.DetermineGameVersionIII();
			}

			// Check if the music player still exists
			if (radioButtonWinamp.Checked)
			{
				readMemory.DeterminePlayerVersionWinamp();
			}
			if (radioButtonFoobar.Checked)
			{
				readMemory.DeterminePlayerVersionFoobar();
			}
			else if (radioButtonOther.Checked)
			{
				readMemory.DeterminePlayerVersionOther();
			}
		}


		void UpdateWindow()
		{
			if (readMemory.actionToTake == ReadMemory.actions.None)
			{
				statusLbl.Text = "Tool not configured";
				versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
				displayedText = displayedTexts.Unitialized;
			}
			else if (readMemory.playerStatus == ReadMemory.statuses.Shutdown)
			{
				statusLbl.Text = "Music player not running";
				versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
				displayedText = displayedTexts.NoMusicPlayer;
			}
			else if (readMemory.playerStatus == ReadMemory.statuses.Error)
			{
				statusLbl.Text = "ERROR";
				versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
			}
			else if (readMemory.gameStatus == ReadMemory.statuses.Shutdown && displayedText != displayedTexts.Shutdown)
			{
				statusLbl.Text = "Game not running";
				versionLbl.Text = "";
				displayedText = displayedTexts.Shutdown;
			}
			else if (readMemory.gameStatus == ReadMemory.statuses.Unrecognized && displayedText != displayedTexts.Unrecognized)
			{
				statusLbl.Text = "Unable to detect game version";
				versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
				displayedText = displayedTexts.Unrecognized;
			}
			else if (readMemory.gameStatus == ReadMemory.statuses.Unconfirmed)
			{
				if (readMemory.radioActive)
				{
					statusLbl.Text = "radio ON (I think)";
					versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
					//displayedText = displayedTexts.Unconfirmed;
				}
				else
				{
					statusLbl.Text = "radio OFF (I think)";
					versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
					//displayedText = displayedTexts.Unconfirmed;
				}
			}
			else if (readMemory.gameStatus == ReadMemory.statuses.Running)
			{
				if (readMemory.radioActive)
				{
					statusLbl.Text = "radio ON - " + readMemory.radioStatus_g;
					versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
					//displayedText = displayedTexts.Running;
				}
				else
				{
					statusLbl.Text = "radio OFF - " + readMemory.radioStatus_g;
					versionLbl.Text = "V" + readMemory.major + "." + readMemory.minor + " " + readMemory.region;
					//displayedText = displayedTexts.Running;
				}
			}
			if (radioButtonWinamp.Checked || radioButtonFoobar.Checked)
			{
				labelVolume.Text = "Volume: " + readMemory.maxVolume;
			}
			else
			{
				labelVolume.Text = null;
			}
		}

		// print some status stuff
		/*void UpdateWindowOld() {
            if (Program.radioStatus == 2) {
                label1.Text = "Radio ON with volume " + Program.volumeStatus;
                label2.Text = Program.volumeStatus.ToString();
                displayedText = 2;
            }
            else if (Program.radioStatus == 7 && displayedText != 7) {
                label1.Text = "Radio OFF";
                displayedText = 7;
            }
            else if (Program.radioStatus == -1 && displayedText != -1) {
                label1.Text = "Winamp not running";
                displayedText = -1;
            }
            else if (Program.radioStatus == -2 && displayedText != -2) {
                label1.Text = "GTASA not running";
                displayedText = -2;
            }
        }*/

		// timer that checks every second for updates to be made to the printed info on the window
		public void WindowTimer()
		{
			timer2 = new Timer();
			timer2.Tick += new EventHandler(timer2Tick);
			timer2.Interval = 1000;
			timer2.Start();
		}

		void timer2Tick(object sender, EventArgs e)
		{
			UpdateWindow();
			CheckGame();
		}

		#region unsorted list music players + action buttons
		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.quickVolume = quickVolumeCk.Checked;
			readMemory.maxVolumeWriteable = false;
		}

		private void radioButtonVolume_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.actionToTake = ReadMemory.actions.Volume;
			quickVolumeCk.Enabled = radioButtonVolume.Checked;
			readMemory.maxVolumeWriteable = false;
			ignoreModsCk.Enabled = radioButtonVolume.Checked;
		}

		private void radioButtonPause_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.isPaused = false;
			if (radioButtonPause.Checked)
			{
				readMemory.actionToTake = ReadMemory.actions.Pause;
			}
			readMemory.maxVolumeWriteable = false;
		}

		private void radioButtonMute_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.isMuted = false;
			if (radioButtonMute.Checked)
			{
				readMemory.actionToTake = ReadMemory.actions.Mute;
			}
			readMemory.maxVolumeWriteable = false;
		}

		private void radioButtonOther_CheckedChanged(object sender, EventArgs e)
		{
			if (radioButtonOther.Checked)
			{
				readMemory.musicP = ReadMemory.musicPlayers.Other;
				radioButtonVolume.Enabled = !radioButtonOther.Checked;
				radioButtonMute.Enabled = !radioButtonOther.Checked;
				radioButtonPause.Enabled = radioButtonOther.Checked;
				readMemory.maxVolumeWriteable = false;
			}
		}

		private void radioButtonWinamp_CheckedChanged(object sender, EventArgs e)
		{
			if (radioButtonWinamp.Checked)
			{
				radioButtonVolume.Enabled = radioButtonWinamp.Checked;
				radioButtonMute.Enabled = radioButtonWinamp.Checked;
				radioButtonPause.Enabled = radioButtonWinamp.Checked;
				readMemory.musicP = ReadMemory.musicPlayers.Winamp;
				readMemory.maxVolumeWriteable = false;
				readMemory.DeterminePlayerVersionWinamp();
				readMemory.maxVolume = readMemory.checkMP3PlayerStatus();
			}
		}

		private void radioButtonFoobar_CheckedChanged(object sender, EventArgs e)
		{
			if (radioButtonFoobar.Checked)
			{
				radioButtonVolume.Enabled = radioButtonFoobar.Checked;
				radioButtonMute.Enabled = radioButtonFoobar.Checked;
				radioButtonPause.Enabled = radioButtonFoobar.Checked;
				readMemory.musicP = ReadMemory.musicPlayers.Foobar;
				readMemory.maxVolumeWriteable = false;
				readMemory.DeterminePlayerVersionFoobar();
				readMemory.maxVolume = readMemory.checkMP3PlayerStatus();
			}
		}


		private void radioButtonVolume_EnabledChanged(object sender, EventArgs e)
		{
			if (radioButtonVolume.Checked)
			{
				radioButtonVolume.Checked = false;
				readMemory.actionToTake = ReadMemory.actions.None;
				radioButtonPause.Checked = true;
				readMemory.DeterminePlayerVersionOther();
			}
		}
		#endregion

		#region game radio buttons
		private void radioButtonIII_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.DetermineGameVersionIII();
			readMemory.game = ReadMemory.games.III;
			emergencyCk.Enabled = true;
			emergencyCk.Checked = true;
			radioCk.Enabled = true;
			radioCk.Checked = true;
			interiorCk.Enabled = false;
			interiorCk.Checked = false;
			menuCk.Enabled = true;
			//checkBoxD.Checked = false;
			kaufmanCk.Enabled = false;
			kaufmanCk.Checked = false;
			announcerCk.Enabled = true;
			announcerCk.Checked = false;
			readMemory.maxVolumeWriteable = false;
			readMemory.p = null;
			readMemory.ShutdownGame();
		}

		private void radioButtonVC_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.DetermineGameVersionVC();
			readMemory.game = ReadMemory.games.VC;
			emergencyCk.Enabled = true;
			emergencyCk.Checked = true;
			radioCk.Enabled = true;
			radioCk.Checked = true;
			interiorCk.Enabled = false;
			interiorCk.Checked = false;
			menuCk.Enabled = true;
			//checkBoxD.Checked = false;
			kaufmanCk.Enabled = true;
			kaufmanCk.Checked = true;
			announcerCk.Enabled = true;
			announcerCk.Checked = false;
			readMemory.maxVolumeWriteable = false;
			readMemory.p = null;
			readMemory.ShutdownGame();
		}

		private void radioButtonSA_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.DetermineGameVersionSA();
			readMemory.game = ReadMemory.games.SA;
			emergencyCk.Enabled = false;
			emergencyCk.Checked = true;
			radioCk.Enabled = false;
			radioCk.Checked = true;
			interiorCk.Enabled = false;
			interiorCk.Checked = true;
			menuCk.Enabled = false;
			menuCk.Checked = true;
			kaufmanCk.Enabled = false;
			kaufmanCk.Checked = false;
			announcerCk.Enabled = false;
			announcerCk.Checked = false;
			readMemory.maxVolumeWriteable = false;
			readMemory.p = null;
			readMemory.ShutdownGame();
		}
		#endregion

		#region when buttons
		private void checkBoxA_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.radioPlayDuringEmergency = emergencyCk.Checked;
			readMemory.maxVolumeWriteable = false;
		}

		private void checkBoxB_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.radioPlayDuringRadio = radioCk.Checked;
			readMemory.maxVolumeWriteable = false;
		}

		private void checkBoxC_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void checkBoxD_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.radioPlayDuringPauseMenu = menuCk.Checked;
			readMemory.maxVolumeWriteable = false;
		}

		private void checkBoxE_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.radioPlayDuringKaufman = kaufmanCk.Checked;
			readMemory.maxVolumeWriteable = false;
		}

		private void checkBoxF_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.radioPlayDuringAnnouncement = announcerCk.Checked;
			readMemory.maxVolumeWriteable = false;
		}



		#endregion

		private void checkBox7_CheckedChanged(object sender, EventArgs e)
		{
			readMemory.ignoreMods = ignoreModsCk.Checked;
		}



		#region configuration

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{

			try
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

				if (radioButtonSA.Checked)
				{
					config.AppSettings.Settings["gameSet"].Value = "SA";
				}


				//game
				if (radioButtonSA.Checked)
				{
					config.AppSettings.Settings["gameSet"].Value = "SA";
				}
				else if (radioButtonVC.Checked)
				{
					config.AppSettings.Settings["gameSet"].Value = "VC";
				}
				else if (radioButtonIII.Checked)
				{
					config.AppSettings.Settings["gameSet"].Value = "III";
				}
				//player
				if (radioButtonWinamp.Checked)
				{
					config.AppSettings.Settings["playerSet"].Value = "Winamp";
				}
				else if (radioButtonFoobar.Checked)
				{
					config.AppSettings.Settings["playerSet"].Value = "Foobar";
				}
				else if (radioButtonVolume.Checked)
				{
					config.AppSettings.Settings["playerSet"].Value = "Other";
				}
				//action
				if (radioButtonMute.Checked)
				{
					config.AppSettings.Settings["actionSet"].Value = "Mute";
				}
				else if (radioButtonPause.Checked)
				{
					config.AppSettings.Settings["actionSet"].Value = "Pause";
				}
				else if (radioButtonVolume.Checked)
				{
					config.AppSettings.Settings["actionSet"].Value = "Volume";
				}
				//action-settings
				config.AppSettings.Settings["quickvolumeSet"].Value = quickVolumeCk.Checked.ToString();
				config.AppSettings.Settings["ignoremodifiersSet"].Value = ignoreModsCk.Checked.ToString();
				//when
				config.AppSettings.Settings["emergencySet"].Value = emergencyCk.Checked.ToString();
				config.AppSettings.Settings["radioSet"].Value = radioCk.Checked.ToString();
				config.AppSettings.Settings["interiorsSet"].Value = interiorCk.Checked.ToString();
				config.AppSettings.Settings["announcerSet"].Value = announcerCk.Checked.ToString();
				config.AppSettings.Settings["kaufmanSet"].Value = kaufmanCk.Checked.ToString();
				config.AppSettings.Settings["menuSet"].Value = menuCk.Checked.ToString();

				config.Save(ConfigurationSaveMode.Modified);
			}
			catch (NullReferenceException ex)
			{
				//Debug.WriteLine("Error writing app settings");
				log.Error(ex, "Error writing app settings");
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			try
			{
				log.Info("loading config");
				var gameSet = GetStringSetting("gameSet");
				var playerSet = GetStringSetting("playerSet");
				var actionSet = GetStringSetting("actionSet");

				switch (gameSet)
				{
					case "SA":
						radioButtonSA.Checked = true;
						break;
					case "VC":
						radioButtonVC.Checked = true;
						break;
					case "III":
						radioButtonIII.Checked = true;
						break;
					default:
						break;
				}

				switch (playerSet)
				{
					case "Winamp":
						radioButtonWinamp.Checked = true;
						break;
					case "Foobar":
						radioButtonFoobar.Checked = true;
						break;
					case "Other":
						radioButtonOther.Checked = true;
						break;
					default:
						break;
				}

				switch (actionSet)
				{
					case "Mute":
						radioButtonMute.Checked = true;
						break;
					case "Pause":
						radioButtonPause.Checked = true;
						break;
					case "Volume":
						radioButtonVolume.Checked = true;
						break;
					default:
						break;
				}

				quickVolumeCk.Checked = GetBoolSetting("quickvolumeSet");
				ignoreModsCk.Checked = GetBoolSetting("ignoremodifiersSet");
				emergencyCk.Checked = GetBoolSetting("emergencySet");
				radioCk.Checked = GetBoolSetting("radioSet");
				interiorCk.Checked = GetBoolSetting("interiorsSet");
				announcerCk.Checked = GetBoolSetting("announcerSet");
				kaufmanCk.Checked = GetBoolSetting("kaufmanSet");
				menuCk.Checked = GetBoolSetting("menuSet");

				readMemory.radioPlayDuringAnnouncement = announcerCk.Checked;
				readMemory.radioPlayDuringEmergency = emergencyCk.Checked;
				readMemory.radioPlayDuringInterior = interiorCk.Checked;
				readMemory.radioPlayDuringKaufman = kaufmanCk.Checked;
				readMemory.radioPlayDuringPauseMenu = menuCk.Checked;
				readMemory.radioPlayDuringRadio = radioCk.Checked;
			}
			catch (Exception ex)
			{
				//Debug.WriteLine("Error reading app settings");
				log.Error(ex, "Error reading app settings");
			}
		}





		#endregion

		#region Configuration helpers

		private string GetStringSetting(string settingName)
		{
			return ConfigurationManager.AppSettings[settingName];
		}

		private bool GetBoolSetting(string settingName, bool defaultValue = false)
		{
			var strVal = GetStringSetting(settingName);
			var parsed = bool.TryParse(strVal, out bool result);
			return parsed ? result : defaultValue;
		}

		#endregion

		private void babyRadio_CheckedChanged(object sender, EventArgs e)
		{
			if (babyRadio.Checked)
			{
				readMemory.actionToTake = ReadMemory.actions.Baby;
			}
		}
	}
}
