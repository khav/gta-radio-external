﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTASARadioExternal
{
	// Keep playing music when no game is loaded

	public class NoGame : GtaGame
	{
		public NoGame() : base(null, 0)
		{

		}

		public override bool ShouldRadioPlay()
		{
			return true;
		}

		public override float DesiredVolume()
		{
			return 1.0f;
		}
	}
}
