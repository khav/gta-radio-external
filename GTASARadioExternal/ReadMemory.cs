﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using NLog;

namespace GTASARadioExternal
{
	public class ReadMemory
	{
		private Process gameProcess;

		private static readonly GtaGame DEFAULT_GAME = new NoGame();
		public GtaGame CurrentGame { get; private set; }

		private static readonly Logger log = LogManager.GetCurrentClassLogger();

		private const int RADIO_ERROR = -1;

		private const int GTA3_RADIO_OFF = 11; // includes ambulance, fire, ice cream truck
		private const int GTA3_RADIO_OFF2 = 12; // seems to be used in 2nd/3rd island
		private const int GTA3_RADIO_HEAD = 0;
		private const int GTA3_RADIO_CLEF = 1;
		private const int GTA3_RADIO_JAH = 2;
		private const int GTA3_RADIO_RISE = 3;
		private const int GTA3_RADIO_LIPS = 4;
		private const int GTA3_RADIO_GAME = 5;
		private const int GTA3_RADIO_MSX = 6;
		private const int GTA3_RADIO_FLB = 7;
		private const int GTA3_RADIO_CHAT = 8;
		private const int GTA3_RADIO_MP3 = 9; // probably
		private const int GTA3_RADIO_COP = 10;
		private const int GTA3_RADIO_MENU = 197; // also includes cutscenes and mission success jingle
		private const int GTA3_RADIO_ALERT1 = 13; // inferred from code
		private const int GTA3_RADIO_ALERT2 = 14; // inferred from code

		private const int GTAVC_RADIO_OFF = 10;
		private const int GTAVC_RADIO_OFF2 = 11;
		private const int GTAVC_RADIO_OFF3 = 12; // ?
		private const int GTAVC_RADIO_WILD = 0;
		private const int GTAVC_RADIO_FLASH = 1;
		private const int GTAVC_RADIO_CHAT = 2;
		private const int GTAVC_RADIO_FEVER = 3;
		private const int GTAVC_RADIO_ROCK = 4;
		private const int GTAVC_RADIO_VCPR = 5;
		private const int GTAVC_RADIO_ESP = 6;
		private const int GTAVC_RADIO_EMO = 7;
		private const int GTAVC_RADIO_WAVE = 8;
		private const int GTAVC_RADIO_MP3 = 9; // probably
		private const int GTAVC_RADIO_COP = 23;
		private const int GTAVC_RADIO_MENU = 201; // from my vc
		private const int GTAVC_RADIO_MENU2 = 1255; // from code
		private const int GTAVC_RADIO_KAUFMAN = 24; // from code
		private const int GTAVC_RADIO_ALERT1 = 25; // inferred from code
		private const int GTAVC_RADIO_ALERT2 = 26; // inferred from code
		private const int GTAVC_RADIO_MALL = 16; // TODO mall radio


		public int major = 0;
		public int minor = 0;
		public enum regionTypes { GTA, US, Europe, Japan, Steam };
		public regionTypes region = regionTypes.GTA;
		public enum statuses { Unitialized, Shutdown, Running, Unrecognized, Unconfirmed, Playing, Silent, Error }
		public statuses gameStatus = statuses.Unitialized;
		public enum games { None, III, VC, SA }
		public games game = games.None;
		public enum musicPlayers { None, Winamp, Foobar, Other }
		public musicPlayers musicP = musicPlayers.None;
		public statuses playerStatus = statuses.Unitialized;
		public Process[] p;
		public Process[] q;
		public int address_radio = 0x0;     // The address of the int that changes depending on radio status
		public int address_volume = 0x0;    // The address of the int that reads the volume of the music player
		public int address_running = 0x0;   // The address of the int that reads whether the music player is on or not
		public int address_base = 0x0;
		public string executable_location;  // Executable location
		public int window_name;         // Window name
		Timer timer1;
		public bool maxVolumeWriteable = true;
		public bool quickVolume = false;
		public bool isPaused = false;
		public bool isMuted = false;
		public enum actions { None, Volume, Mute, Pause, Baby }
		public actions actionToTake;

		public int failSafeAttempts = 0;

		public int prevVolumeStatus = -1;
		public int volumeStatus;
		public int radioStatus_g;
		public int prevRadioStatus;
		public int maxVolume;
		public int babyVolume;
		public bool radioActive = false;
		public bool ignoreMods = false;

		//public int radioLowerBoundary = 0;
		//public int radioUpperBoundary = 10;
		public bool radioPlayDuringPauseMenu;
		public bool radioPlayDuringRadio;
		public bool radioPlayDuringEmergency;
		public bool radioPlayDuringKaufman;
		public bool radioPlayDuringAnnouncement;
		public bool radioPlayDuringInterior;



		// Dont know what this does
		const int PROCESS_WM_READ = 0x0010;

		// Allows me to read memory from processes
		[DllImport("kernel32.dll")]
		public static extern Int32 ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress,
		  [In, Out] byte[] buffer, UInt32 size, out IntPtr lpNumberOfBytesRead);

		// Send keystrokes for volume
		[DllImport("user32.dll")]
		public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);

		[DllImport("user32.dll")]
		public static extern int SendMessage(int hwnd, int wMsg, int wParam, int lParam);

		[DllImport("user32.dll")]
		public static extern int FindWindow(string lpClassName, String lpWindowName);


		public ReadMemory()
		{
			CurrentGame = DEFAULT_GAME;
		}


		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
					* Game Detection
		* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		public enum Gta { III, VC, SA }

		public class GameParams
		{
			public struct MagicNumbers
			{
				public readonly int addr;
				public readonly int val;
				public readonly int radio;
				public readonly int index;
				public MagicNumbers(int addr, int val, int radio, int index)
				{
					this.addr = addr;
					this.val = val;
					this.radio = radio;
					this.index = index;
				}

				public override bool Equals(object obj) => obj is MagicNumbers && this == (MagicNumbers)obj;
				public override int GetHashCode() => addr.GetHashCode() ^ val.GetHashCode() ^ radio.GetHashCode() ^ index.GetHashCode();
				public static bool operator ==(MagicNumbers x, MagicNumbers y) => x.addr == y.addr && x.val == y.val && x.radio == y.radio && x.index == y.index;
				public static bool operator !=(MagicNumbers x, MagicNumbers y) => !(x == y);
			}

			public GameParams(Gta which)
			{
				var pns = new List<string>();
				var mgs = new List<MagicNumbers>();

				switch (which)
				{
					case Gta.III:
						pns.Add("gta3");
						mgs.Add(new MagicNumbers(0x5C1E70, 1407551829, 0x8F3967, 0));
						mgs.Add(new MagicNumbers(0x5C2130, 1407551829, 0x8F3A1B, 1));
						mgs.Add(new MagicNumbers(0x5C6FD0, 1407551829, 0x903B5C, 2));
						break;
					case Gta.VC:
						pns.Add("gta-vc");
						mgs.Add(new MagicNumbers(0x667BF0, 1407551829, 0x9839C0, 0));
						mgs.Add(new MagicNumbers(0x667C40, 1407551829, 0x9839C0, 1));
						mgs.Add(new MagicNumbers(0xA402ED, 1448235347, 0x9829C8, 2));
						mgs.Add(new MagicNumbers(0xACD0A2, 1793887061, 0x9809D0, 3));
						break;
					case Gta.SA:
						pns.Add("gta-sa");
						pns.Add("gta_sa");
						mgs.Add(new MagicNumbers(0x82457C, 38079, 0x008CB760, 0));
						mgs.Add(new MagicNumbers(0x8245BC, 38079, 0x008CB760, 1));
						mgs.Add(new MagicNumbers(0x8252FC, 38079, 0x008CCFE8, 2));
						mgs.Add(new MagicNumbers(0x82533C, 38079, 0x008CCFE8, 3));
						mgs.Add(new MagicNumbers(0x85EC4A, 38079, 0x0093AB68, 4));
						break;
				}

				ProcessNames = pns;
				Magic = mgs;
			}

			public IReadOnlyCollection<string> ProcessNames { get; private set; }
			public IReadOnlyList<MagicNumbers> Magic { get; private set; }

		}

		private static readonly IDictionary<Gta, GameParams> GTA_PARAMS = new Dictionary<Gta, GameParams>();

		private static GameParams GetGameParams(Gta which)
		{
			if (!GTA_PARAMS.ContainsKey(which))
			{
				GTA_PARAMS.Add(which, new GameParams(which));
			}

			return GTA_PARAMS[which];
		}

		private Process GetGameProcess(IReadOnlyCollection<string> procNames)
		{
			p = procNames.SelectMany(pn => Process.GetProcessesByName(pn)).ToArray();

			Process proc = procNames.SelectMany(pn => Process.GetProcessesByName(pn)).FirstOrDefault();
			return proc;
		}

		public void ShutdownGame()
		{
			gameProcess = null;
			gameStatus = statuses.Shutdown;
			CurrentGame = DEFAULT_GAME;
		}

		public void DetermineGameVersion(Gta which)
		{
			log.Debug("Detecting game {0}", which);

			try
			{
				var game = GetGameParams(which);

				var proc = GetGameProcess(game.ProcessNames);

				if (proc != null)
				{
					if (gameProcess?.Id == proc?.Id && gameStatus == statuses.Running)
					{
						log.Debug("GTA {0} process {1}", which, proc.Id);
					}
					else
					{
						log.Debug("Found GTA {1} process {0}", proc?.Id, which);
						gameProcess = proc;

						var ourMagic = game.Magic.FirstOrDefault(m =>
						{
							var val = ReadValue(proc.Handle, m.addr, false, true);
							return val == m.val;
						});

						if (ourMagic == default(GameParams.MagicNumbers))
						{
							gameStatus = statuses.Unrecognized;
						}
						else
						{
							//TODO set major, minor, region
							gameStatus = statuses.Running;
							address_radio = ourMagic.radio;

							if (which == Gta.III)
							{
								CurrentGame = new Gta3Game(proc, ourMagic.radio)
								{
									PlayDuringAlert = radioPlayDuringAnnouncement,
									PlayDuringRadio = radioPlayDuringRadio,
									PlayInMenu = radioPlayDuringPauseMenu,
									PlayInPoliceCar = radioPlayDuringEmergency,
									PlayWhileOff = false
								};

								
							}
						}
					}
				}
				else
				{
					log.Debug("Couldn't find game process");
					ShutdownGame();
				}
			}
			catch (Exception ex)
			{
				// TODO put back specific exception handling if necessary
				// for InvalidOperationException, NullReferenceException, IndexOutOfRangeException 
				log.Error(ex, "Exception while detecting game");
				ShutdownGame();
			}
		}

		public void DetermineGameVersionIII() => DetermineGameVersion(Gta.III);

		public void DetermineGameVersionVC() => DetermineGameVersion(Gta.VC);

		public void DetermineGameVersionSA() => DetermineGameVersion(Gta.SA);

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
						* Music Player Detection
		* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		// Determine the version of Winamp
		public void DeterminePlayerVersionWinamp()
		{
			q = Process.GetProcessesByName("winamp");
			if (q.Length != 0)
			{
				playerStatus = statuses.Running;
				address_volume = 0x07AA9D9C;
				address_running = 0x4BF3EC;
				window_name = FindWindow("Winamp v1.x", null);
			}
			else
			{
				playerStatus = statuses.Shutdown;
			}
		}

		// Determine the version of Foobar
		public void DeterminePlayerVersionFoobar()
		{
			q = Process.GetProcessesByName("foobar2000");
			if (q.Length != 0)
			{
				playerStatus = statuses.Running;
				address_base = q[0].MainModule.BaseAddress.ToInt32();
				address_volume = address_base + 0x18C438;
				address_running = address_base + 0x18B1F0;
				executable_location = q[0].MainModule.FileName;
			}
			else
			{
				playerStatus = statuses.Shutdown;
			}
		}

		// This isn't actually going to detect anything
		public void DeterminePlayerVersionOther()
		{
			playerStatus = statuses.Running;
		}


		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
					* Radio Status Detection
		* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		private int GetRadioStatus()
		{
			var result = radioStatus_g;

			try
			{
				result = ReadValue(gameProcess.Handle, address_radio, false, false);
				//log.Debug("Successfully read radio status {0}", result);
				if (result != radioStatus_g)
				{
					log.Debug("Radio changed from {0} to {1}", radioStatus_g, result);
					radioStatus_g = result;
				}
			}
			catch (Exception ex)
			{
				// TODO put back specific exception handling if necessary
				// for InvalidOperationException, NullReferenceException, IndexOutOfRangeException 
				log.Error(ex, "Exception while reading radio status");
				result = RADIO_ERROR;
				ShutdownGame();
			}

			return result;
		}

		private void UpdateRadioIII()
		{
			if (gameStatus != statuses.Running && gameStatus != statuses.Unconfirmed)
			{
				return;
			}

			var radioStatus = GetRadioStatus();

			switch (radioStatus)
			{
				case GTA3_RADIO_CHAT:
				case GTA3_RADIO_CLEF:
				case GTA3_RADIO_FLB:
				case GTA3_RADIO_GAME:
				case GTA3_RADIO_HEAD:
				case GTA3_RADIO_JAH:
				case GTA3_RADIO_LIPS:
				case GTA3_RADIO_MSX:
				case GTA3_RADIO_RISE:
				case GTA3_RADIO_MP3:
					SwitchRadio(radioPlayDuringRadio);
					break;
				case GTA3_RADIO_MENU:
					SwitchRadio(radioPlayDuringPauseMenu);
					break;
				case GTA3_RADIO_COP:
					SwitchRadio(radioPlayDuringEmergency);
					break;
				case GTA3_RADIO_ALERT1:
				case GTA3_RADIO_ALERT2:
					SwitchRadio(radioPlayDuringAnnouncement);
					break;
				case GTA3_RADIO_OFF:
				case GTA3_RADIO_OFF2:
					SwitchRadio(false);
					break;
				case RADIO_ERROR:
				default:
					log.Warn("Unexpected radio status {0}", radioStatus);
					break;
			}
		}

		private void UpdateRadioVC()
		{
			if (gameStatus != statuses.Running && gameStatus != statuses.Unconfirmed)
			{
				return;
			}

			var radioStatus = GetRadioStatus();

			switch (radioStatus)
			{
				case GTAVC_RADIO_CHAT:
				case GTAVC_RADIO_EMO:
				case GTAVC_RADIO_ESP:
				case GTAVC_RADIO_FEVER:
				case GTAVC_RADIO_FLASH:
				case GTAVC_RADIO_ROCK:
				case GTAVC_RADIO_VCPR:
				case GTAVC_RADIO_WAVE:
				case GTAVC_RADIO_WILD:
				case GTAVC_RADIO_MP3:
					SwitchRadio(radioPlayDuringRadio);
					break;
				case GTAVC_RADIO_MENU:
				case GTAVC_RADIO_MENU2:
					SwitchRadio(radioPlayDuringPauseMenu);
					break;
				case GTAVC_RADIO_COP:
					SwitchRadio(radioPlayDuringEmergency);
					break;
				case GTAVC_RADIO_ALERT1:
				case GTAVC_RADIO_ALERT2:
					SwitchRadio(radioPlayDuringAnnouncement);
					break;
				case GTAVC_RADIO_KAUFMAN:
					SwitchRadio(radioPlayDuringKaufman);
					break;
				case GTAVC_RADIO_OFF:
				case GTAVC_RADIO_OFF2:
				case GTAVC_RADIO_OFF3:
					SwitchRadio(false);
					break;
				case RADIO_ERROR:
				default:
					log.Warn("Unexpected radio status {0}", radioStatus);
					break;
			}
		}

		private void SwitchRadio(bool radioOn)
		{
			switch (actionToTake)
			{
				case actions.Mute:
					if (isMuted == radioOn)
					{
						log.Debug("Changing radio to {0} (mute method)", radioOn);
						isMuted = !radioOn;
						RadioChangerMute(isMuted);
					}
					break;
				case actions.Pause:
					if (isPaused == radioOn)
					{
						log.Debug("Changing radio to {0} (pause method)", radioOn);
						isPaused = !radioOn;
						RadioChangerPause(isPaused);
					}
					break;
				case actions.Volume:
					log.Warn("Volume method N/A!");
					break;
				case actions.Baby:
					CurrentGame?.ShouldRadioPlay(); // we know it should, but we need to read radio status
					var desired = CurrentGame?.DesiredVolume();
					if (desired.HasValue)
					{
						var newv = (int)(maxVolume * desired.Value);
						SetWinampVolume(newv);
					}
					break;
				case actions.None:
					log.Warn("No action set");
					break;
			}
		}


		public void CheckRadioStatusIII()
		{
			if (playerStatus != statuses.Running)
			{
				return;
			}



			int radioStatus;

			if (actionToTake == actions.Pause)
			{
				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, false);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException A");
					//	log.Error(ex, "InvalidOperationException A");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException A");
					//	log.Error(ex, "NullReferenceException A");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException A");
					//	log.Error(ex, "IndexOutOfRangeException A");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}


					#region pause and unpause condition clauses
					if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 10 && radioPlayDuringEmergency == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 197 && radioPlayDuringPauseMenu == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					if (radioStatus >= 13 && radioStatus <= 14 && radioPlayDuringAnnouncement == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}


					else if (radioStatus == 197 && radioPlayDuringPauseMenu == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 10 && radioPlayDuringEmergency == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus >= 13 && radioStatus <= 14 && radioPlayDuringAnnouncement == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}

					else if (radioStatus > 10 && radioStatus != 197 && radioStatus != 13 && radioStatus != 14 && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					#endregion
				}
			}
			else if (actionToTake == actions.Volume)
			{
				//TODO refactor this section
				radioStatus = radioStatus_g;

				// Unless the radio is currently changing, allow user to change volume
				#region maxvolume changer
				if (gameStatus != statuses.Running && gameStatus != statuses.Unconfirmed)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus == 10 && radioPlayDuringEmergency == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus == 197 && radioPlayDuringPauseMenu == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				#endregion
				prevRadioStatus = radioStatus;

				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//#region try catch radiostatus
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, false);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException B");
					//	log.Error(ex, "InvalidOperationException B");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException B");
					//	log.Error(ex, "NullReferenceException B");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException B");
					//	log.Error(ex, "IndexOutOfRangeException B");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//#endregion

					volumeStatus = checkMP3PlayerStatus();
					RadioChangerVolume(radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio
											|| radioStatus == 10 && radioPlayDuringEmergency == true
											|| radioStatus >= 13 && radioStatus <= 14 && radioPlayDuringAnnouncement == true
											|| radioStatus == 197 && radioPlayDuringPauseMenu == true
					);
				}
			}
			else if (actionToTake == actions.Mute)
			{
				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//#region try catch radiostatus
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, false);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException K");
					//	log.Error(ex, "InvalidOperationException K");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException K");
					//	log.Error(ex, "NullReferenceException K");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException K");
					//	log.Error(ex, "IndexOutOfRangeException K");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//#endregion

					#region mute and unmute condition clauses
					if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 10 && radioPlayDuringEmergency == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 197 && radioPlayDuringPauseMenu == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					if (radioStatus >= 13 && radioStatus <= 14 && radioPlayDuringAnnouncement == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}


					else if (radioStatus == 197 && radioPlayDuringPauseMenu == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 10 && radioPlayDuringEmergency == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus >= 13 && radioStatus <= 14 && radioPlayDuringAnnouncement == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}

					else if (radioStatus > 10 && radioStatus != 197 && radioStatus != 13 && radioStatus != 14 && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					#endregion
				}
			}
		}

		public void CheckRadioStatusVC()
		{
			if (playerStatus != statuses.Running)
			{
				return;
			}

			int radioStatus;

			if (actionToTake == actions.Pause)
			{
				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, true);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException C");
					//	log.Error(ex, "InvalidOperationException C");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException C");
					//	log.Error(ex, "NullReferenceException C");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException C");
					//	log.Error(ex, "IndexOutOfRangeException C");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}

					if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					if (radioStatus >= 25 && radioStatus <= 26 && radioPlayDuringAnnouncement == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 23 && radioPlayDuringEmergency == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 1225 && radioPlayDuringPauseMenu == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 24 && radioPlayDuringKaufman == true && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}

					else if (radioStatus == 1225 && radioPlayDuringPauseMenu == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus >= 25 && radioStatus <= 26 && radioPlayDuringAnnouncement == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 23 && radioPlayDuringEmergency == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 24 && radioPlayDuringKaufman == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == false && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}

					else if (radioStatus > 9 && radioStatus < 23 && isPaused == false || radioStatus > 26 && radioStatus != 1225 && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
				}
			}
			else if (actionToTake == actions.Volume)
			{
				//TODO refactor this
				radioStatus = radioStatus_g;

				// Unless the radio is currently changing, allow user to change volume
				if (gameStatus != statuses.Running && gameStatus != statuses.Unconfirmed)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus >= 25 && radioStatus <= 26 && radioPlayDuringAnnouncement == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus == 23 && radioPlayDuringEmergency == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus == 24 && radioPlayDuringKaufman == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				else if (maxVolumeWriteable && radioStatus == 1225 && radioPlayDuringPauseMenu == true)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				prevRadioStatus = radioStatus;

				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, true);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException D");
					//	log.Error(ex, "InvalidOperationException D");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException D");
					//	log.Error(ex, "NullReferenceException D");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException D");
					//	log.Error(ex, "IndexOutOfRangeException D");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}

					volumeStatus = checkMP3PlayerStatus();
					RadioChangerVolume(radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio
										|| radioStatus == 23 && radioPlayDuringEmergency == true
										|| radioStatus == 24 && radioPlayDuringKaufman == true
										|| radioStatus >= 25 && radioStatus <= 26 && radioPlayDuringAnnouncement == true
										|| radioStatus == 1225 && radioPlayDuringPauseMenu == true
					);
				}
			}
			else if (actionToTake == actions.Mute)
			{
				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, true);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException I");
					//	log.Error(ex, "InvalidOperationException I");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException I");
					//	log.Error(ex, "NullReferenceException I");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException I");
					//	log.Error(ex, "IndexOutOfRangeException I");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}


					if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					if (radioStatus >= 25 && radioStatus <= 26 && radioPlayDuringAnnouncement == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 23 && radioPlayDuringEmergency == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 1225 && radioPlayDuringPauseMenu == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 24 && radioPlayDuringKaufman == true && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}

					else if (radioStatus == 1225 && radioPlayDuringPauseMenu == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus >= 25 && radioStatus <= 26 && radioPlayDuringAnnouncement == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 23 && radioPlayDuringEmergency == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 24 && radioPlayDuringKaufman == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus >= 0 && radioStatus <= 9 && radioPlayDuringRadio == false && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}

					else if (radioStatus > 9 && radioStatus < 23 && isMuted == false || radioStatus > 26 && radioStatus != 1225 && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
				}
			}
		}

		public void CheckRadioStatusSA()
		{
			if (playerStatus != statuses.Running)
			{
				return;
			}

			int radioStatus;

			if (actionToTake == actions.Pause)
			{
				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, true);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException H");
					//	log.Error(ex, "InvalidOperationException H");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException H");
					//	log.Error(ex, "NullReferenceException H");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException H");
					//	log.Error(ex, "IndexOutOfRangeException H");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}

					if (radioStatus == 2 && isPaused == true)
					{
						isPaused = false;
						RadioChangerPause(isPaused);
					}
					else if (radioStatus == 7 && isPaused == false)
					{
						isPaused = true;
						RadioChangerPause(isPaused);
					}
				}
			}
			else if (actionToTake == actions.Volume)
			{
				//TODO refactor this
				radioStatus = radioStatus_g;

				// Unless the radio is currently changing, allow user to change volume
				if (maxVolumeWriteable == true && radioStatus == 2 || gameStatus != statuses.Running && gameStatus != statuses.Unconfirmed)
				{
					maxVolume = checkMP3PlayerStatus();
				}
				prevRadioStatus = radioStatus;

				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, true);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException J");
					//	log.Error(ex, "InvalidOperationException J");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException J");
					//	log.Error(ex, "NullReferenceException J");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException J");
					//	log.Error(ex, "IndexOutOfRangeException J");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}

					volumeStatus = checkMP3PlayerStatus();
					RadioChangerVolume(radioStatus == 2);
				}
			}
			else if (actionToTake == actions.Mute)
			{
				if (gameStatus == statuses.Running || gameStatus == statuses.Unconfirmed)
				{
					radioStatus = GetRadioStatus();
					//try
					//{
					//	radioStatus = ReadValue(p[0].Handle, address_radio, false, true);
					//}
					//catch (InvalidOperationException ex)
					//{
					//	//Debug.WriteLine("InvalidOperationException G");
					//	log.Error(ex, "InvalidOperationException G");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (NullReferenceException ex)
					//{
					//	//Debug.WriteLine("NullReferenceException G");
					//	log.Error(ex, "NullReferenceException G");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (IndexOutOfRangeException ex)
					//{
					//	//Debug.WriteLine("IndexOutOfRangeException G");
					//	log.Error(ex, "IndexOutOfRangeException G");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}
					//catch (Exception ex)
					//{
					//	log.Error(ex, "Other exception");
					//	gameStatus = statuses.Shutdown;
					//	return;
					//}


					if (radioStatus == 2 && isMuted == true)
					{
						isMuted = false;
						RadioChangerMute(isMuted);
					}
					else if (radioStatus == 7 && isMuted == false)
					{
						isMuted = true;
						RadioChangerMute(isMuted);
					}
				}
			}
		}


		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
				* Media Player Controls
		* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		void SetWinampVolume(int volume)
		{
			if (volume != babyVolume)
			{
				log.Debug("Volume set to {0} Baby!", volume);
				SendMessage(window_name, 0x0400, volume, 122);
				babyVolume = volume;
			}
		}

		// Change Radio based on muting/unmuting
		void RadioChangerMute(bool radioOff)
		{
			if (musicP == musicPlayers.Foobar)
			{
				// This only works with foobar and will break if I try to implement this with anything else.
				radioActive = !radioOff;

				ProcessStartInfo psi = new ProcessStartInfo();
				psi.FileName = Path.GetFileName(executable_location);
				psi.WorkingDirectory = Path.GetDirectoryName(executable_location);
				psi.Arguments = "/command:mute";
				Process.Start(psi);
			}
			else if (musicP == musicPlayers.Winamp)
			{
				radioActive = !radioOff;
				if (radioOff)
				{
					maxVolume = checkMP3PlayerStatus();
					SendMessage(window_name, 0x0400, 0, 122);
				}
				else
				{
					SendMessage(window_name, 0x0400, maxVolume, 122);
				}
			}
		}

		// Change Radio based on pausing/unpausing
		void RadioChangerPause(bool radioOff)
		{
			radioActive = !radioOff;
			keybd_event(0xB3, 0, 1, IntPtr.Zero);
			keybd_event(0xB3, 0, 2, IntPtr.Zero);
		}

		// Change Radio based on Volume slider
		void RadioChangerVolume(bool radioOn)
		{
			radioActive = radioOn;
			if (radioOn && volumeStatus < maxVolume)
			{
				maxVolumeWriteable = false;
				if (Control.ModifierKeys == Keys.Shift || Control.ModifierKeys == Keys.Alt || Control.ModifierKeys == Keys.Control)
				{
					if (!ignoreMods)
					{
						return;
					}
				}

				if (volumeStatus == prevVolumeStatus)
				{
					failSafeAttempts += 1;
					if (failSafeAttempts > 10000)
					{
						playerStatus = statuses.Error;
						return;
					}
				}
				// radio should be on but volume is too low
				keybd_event(0xAF, 0, 1, IntPtr.Zero);
				if (quickVolume)
				{
					keybd_event(0xAF, 0, 1, IntPtr.Zero);
					keybd_event(0xAF, 0, 1, IntPtr.Zero);
				}
				keybd_event(0xAF, 0, 2, IntPtr.Zero);


				prevVolumeStatus = volumeStatus;
				volumeStatus = checkMP3PlayerStatus();
			}
			else if (!radioOn && volumeStatus > 0)
			{
				if (Control.ModifierKeys == Keys.Shift || Control.ModifierKeys == Keys.Alt || Control.ModifierKeys == Keys.Control)
				{
					if (!ignoreMods)
					{
						return;
					}
				}
				// radio should be off but volume isn't 0
				if (volumeStatus == prevVolumeStatus)
				{
					failSafeAttempts += 1;
					if (failSafeAttempts > 100000)
					{
						playerStatus = statuses.Error;
						return;
					}
				}
				keybd_event(0xAE, 0, 1, IntPtr.Zero);
				keybd_event(0xAE, 0, 1, IntPtr.Zero);
				keybd_event(0xAE, 0, 1, IntPtr.Zero);
				if (quickVolume)
				{
					keybd_event(0xAE, 0, 1, IntPtr.Zero);
					keybd_event(0xAE, 0, 1, IntPtr.Zero);
					keybd_event(0xAE, 0, 1, IntPtr.Zero);
					keybd_event(0xAE, 0, 1, IntPtr.Zero);
					keybd_event(0xAE, 0, 1, IntPtr.Zero);
					keybd_event(0xAE, 0, 1, IntPtr.Zero);
				}
				keybd_event(0xAE, 0, 2, IntPtr.Zero);
				prevVolumeStatus = volumeStatus;
				volumeStatus = checkMP3PlayerStatus();
			}
			else if (maxVolumeWriteable == false)
			{
				maxVolumeWriteable = true;
				failSafeAttempts = 0;
			}
		}


		// volume slider is used for checking if radio is on or off (winamp didn't want to let me take control of its mute button)
		public int checkMP3PlayerStatus()
		{
			int volumeLevel = -1;
			if (gameStatus != statuses.Running && prevVolumeStatus != -1)
			{
				return maxVolume;
			}
			else if (playerStatus == statuses.Running)
			{
				try
				{
					volumeLevel = ReadValue(q[0].Handle, address_volume, musicP == musicPlayers.Foobar, false);
				}
				catch (InvalidOperationException ex)
				{
					//Debug.WriteLine("InvalidOperationException E");
					log.Error(ex, "InvalidOperationException E");
					playerStatus = statuses.Shutdown;
					return -1;
				}
				catch (NullReferenceException ex)
				{
					//Debug.WriteLine("NullReferenceException E");
					log.Error(ex, "NullReferenceException E");
					playerStatus = statuses.Shutdown;
					return -1;
				}
				catch (IndexOutOfRangeException ex)
				{
					//Debug.WriteLine("IndexOutOfRangeException E");
					log.Error(ex, "IndexOutOfRangeException E");
					playerStatus = statuses.Shutdown;
					return -1;
				}
				catch (Exception ex)
				{
					log.Error(ex, "Other exception");
					playerStatus = statuses.Shutdown;
					return -1;
				}

				// If it returns 255, make sure it isn't glitching, which winamp likes to do if it hasn't been turned on yet
				if (volumeLevel == 255)
				{
					int activity;
					activity = checkMP3ActiveStatus();
					return volumeLevel * activity;
				}
				return volumeLevel;
			}
			else
			{
				return -1;
			}
		}

		// This function checks whether winamp is actually playing. To prevent crash if winamp has just been booted but hasn't started music yet.
		private int checkMP3ActiveStatus()
		{
			int playerActive = 0;
			if (playerStatus == statuses.Running)
			{
				try
				{
					playerActive = ReadValue(q[0].Handle, address_running, musicP == musicPlayers.Foobar, false);
				}
				catch (InvalidOperationException ex)
				{
					//Debug.WriteLine("InvalidOperationException F");
					log.Error(ex, "InvalidOperationException F");
					playerStatus = statuses.Shutdown;
					return 0;
				}
				catch (NullReferenceException ex)
				{
					//Debug.WriteLine("NullReferenceException F");
					log.Error(ex, "NullReferenceException F");
					playerStatus = statuses.Shutdown;
					return 0;
				}
				catch (IndexOutOfRangeException ex)
				{
					//Debug.WriteLine("IndexOutOfRangeException F");
					log.Error(ex, "IndexOutOfRangeException F");
					playerStatus = statuses.Shutdown;
					return 0;
				}
				catch (Exception ex)
				{
					log.Error(ex, "Other exception");
					playerStatus = statuses.Shutdown;
					return 0;
				}
				// how did this ever run?
				//volumeStatus = checkMP3PlayerStatus();
				return playerActive;
			}
			else
			{
				return 0;
			}
		}

		// update radio status periodically
		public void InitTimer()
		{
			timer1 = new Timer();
			timer1.Tick += new EventHandler(Timer1Tick);
			timer1.Interval = 100;
			timer1.Start();
		}

		void Timer1Tick(object sender, EventArgs e)
		{
			if (CurrentGame != null)
			{
				SwitchRadio(CurrentGame.ShouldRadioPlay());
			}
			else
			{
				if (game == games.SA)
				{
					CheckRadioStatusSA();
				}
				else if (game == games.VC)
				{
					UpdateRadioVC();
				}
				else if (game == games.III)
				{
					UpdateRadioIII();
				}
			}
		}


		// Bitconverter to return whatever is in that memory address to an integer so I can work with it
		public static int ReadValue(IntPtr handle, long address, bool floatRequested, bool fourBytes)
		{
			if (floatRequested)
			{
				Single floatInstead = ReadFloat(handle, address);
				return Convert.ToInt32(floatInstead) + 50;
			}
			else if (!fourBytes)
			{
				return (int)ReadBytes(handle, address, 1)[0];
			}
			else
			{
				return BitConverter.ToInt32(ReadBytes(handle, address, 4), 0);
			}
		}

		/*private int ReadInt32(IntPtr handle, long address) {
			if (musicP == musicPlayers.Foobar) {
				Single floatInstead = ReadFloat(handle, address);
				return Convert.ToInt32(floatInstead) + 100;
			}
			return BitConverter.ToInt32(ReadBytes(handle, address, 4), 0);
		}*/

		private static Single ReadFloat(IntPtr handle, long address)
		{
			return BitConverter.ToSingle(ReadBytes(handle, address, 4), 0);
		}

		// Read memory
		private static byte[] ReadBytes(IntPtr handle, long address, uint bytesToRead)
		{
			IntPtr ptrBytesRead;
			byte[] buffer = new byte[bytesToRead];
			ReadProcessMemory(handle, new IntPtr(address), buffer, bytesToRead, out ptrBytesRead);
			return buffer;
		}


	}
}
